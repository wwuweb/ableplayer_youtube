<?php

function ableplayer_youtube_preprocess_ableplayer_youtube_video(&$variables) {
  $variables['data_youtube_id'] = _ableplayer_youtube_get_video_id($variables['uri']);
  $variables['data_translation_path'] = url(libraries_get_path('ableplayer') . '/translations/', array('absolute' => TRUE,));
}

function _ableplayer_youtube_get_video_id($uri) {
  $wrapper = file_stream_wrapper_get_instance_by_uri($uri);
  $parts = $wrapper->get_parameters();
  $id = check_plain($parts['v']);

  return $id;
}
