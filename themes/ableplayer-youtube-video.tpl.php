<?php
/**
 * @file ableplayer-youtube-video.tpl.php
 *
 * Template file for theme('ableplayer_youtube_video')
 */
?>
<video id="ableplayer-youtube-<?php print $id; ?>" width="<?php print $options['width']; ?>" height="<?php print $options['height']; ?>" <?php if ($options['autoplay']): ?>autoplay<?php endif ?> <?php if ($options['preload']): ?>preload<?php endif ?> <?php if ($options['show_now_playing']): ?>data-show-now-playing<?php endif ?> <?php if ($options['use_transcript_button']): ?>data-use-transcript-button<?php endif ?> <?php if ($options['lyrics_mode']): ?>data-lyrics-mode<?php endif ?> data-volume="<?php print $options['volume']; ?>" data-start-time="<?php print $options['start_time']; ?>" data-seek-interval="<?php print $options['seek_interval']; ?>" data-transcript-title="<?php print $options['transcript_title']; ?>" data-translation-path="<?php print $data_translation_path; ?>" data-youtube-id="<?php print $data_youtube_id; ?>" data-debug data-able-player>
</video>
