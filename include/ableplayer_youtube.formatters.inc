<?php

/*
 * Implements hook_file_formatter_info().
 */
function ableplayer_youtube_file_formatter_info() {
  $formatters = array();

  $formatters['ableplayer_youtube_video'] = array(
    'label' => t('Able Player'),
    'description' => t('Display a YouTube video in Able Player'),
    'file types' => array('video'),
    'default settings' => array(
      'width' => 480,
      'height' => 360,
      'autoplay' => FALSE,
      'preload' => FALSE,
      'show_now_playing' => FALSE,
      'lyrics_mode' => FALSE,
      'volume' => 0.5,
      'start_time' => 0,
      'seek_interval' => FALSE,
      'transcript_title' => FALSE,
      'use_transcript_button' => FALSE,
    ),
    'view callback' => 'ableplayer_youtube_file_formatter_video_view',
    'settings callback' => 'ableplayer_youtube_file_formatter_video_settings',
    'mime types' => array('video/youtube'),
  );

  return $formatters;
}

/*
 * Impelemtnts hook_file_formatter_FORMATTER_view().
 *
 * This is a recommended callback naming scheme, not a true hook function.
 */
function ableplayer_youtube_file_formatter_video_view($file, $display, $langcode) {
  $scheme = file_uri_scheme($file->uri);

  if ($scheme == 'youtube' && empty($file->override['wysiwyg'])) {
    $element = array(
      '#theme' => 'ableplayer_youtube_video',
      '#uri' => $file->uri,
      '#options' => array(),
    );

    foreach (array(
      'width',
      'height',
      'autoplay',
      'preload',
      'show_now_playing',
      'lyrics_mode',
      'volume',
      'start_time',
      'seek_interval',
      'transcript_title',
      'use_transcript_button',
    ) as $setting) {
      $element['#options'][$setting] = isset($file->override[$setting]) ?
        $file->override[$setting] :
        $display['settings'][$setting];
    }

    return $element;
  }
}

function ableplayer_youtube_file_formatter_video_settings($form, &$form_state, $settings) {
  $element = array();

  $element['width'] = array(
    '#title' => t('Width'),
    '#type' => 'textfield',
    '#type' => 'textfield',
    '#default_value' => $settings['width'],
    '#description' => t('Width of the media player in pixels. For video, this value should reflect the target width of the media itself.'),
    '#size' => 6,
    '#required' => true,
    '#element_validate' => array('_ableplayer_youtube_validate_dimensions'),
  );

  $element['height'] = array(
    '#title' => t('Height'),
    '#type' => 'textfield',
    '#default_value' => $settings['height'],
    '#description' => t('Height of the video in pixels. If not provided will default to 360.'),
    '#size' => 6,
    '#required' => true,
    '#element_validate' => array('_ableplayer_youtube_validate_dimensions'),
  );

  $element['autoplay'] = array(
    '#title' => t('Autoplay video on load'),
    '#type' => 'checkbox',
    '#default_value' => $settings['autoplay'],
    '#description' => t('Play media automatically when page loads. For accessibility reasons, this is not recommended unless user is sure to expect media to automatically start. For example, autoplay could reasonably be used in conjunction with data-start-time in a media search application.'),
  );

  $element['preload'] = array(
    '#title' => t('Preload video'),
    '#type' => 'checkbox',
    '#default_value' => $settings['preload'],
    '#description' => t('Tells the browser how much media to download when the page loads. If the media is the central focus of the web page, use preload=“auto”, which instructs the browser to download as much of the media as possible. If the media is not a central focus, downloading the entire media resource can consume valuable bandwidth, so preload=“metadata” would be a better option.'),
  );

  $element['show_now_playing'] = array(
    '#title' => t('Show now playing'),
    '#type' => 'checkbox',
    '#default_value' => $settings['show_now_playing'],
    '#description' => t('include "Selected track" section within player; only applies when a playlist is present.'),
  );

  $element['use_transcript_button'] = array(
    '#title' => t('Use transcript button'),
    '#type' => 'checkbox',
    '#default_value' => $settings['use_transcript_button'],
    '#description' => t('Uncheck to exclude transcript button from controller. If using the data-transcript-div attribute to write the transcript to an external container (e.g., on a dedicated transcript page), you might not want users to be able to toggle the transcript off.'),
  );

  $element['lyrics_mode'] = array(
    '#title' => t('Lyrics mode'),
    '#type' => 'checkbox',
    '#default_value' => $settings['lyrics_mode'],
    '#description' => t('Forces a line break between and within captions in the transcript.'),
  );

  $element['volume'] = array(
    '#title' => t('Volume'),
    '#type' => 'textfield',
    '#default_value' => $settings['volume'],
    '#size' => 6,
    '#description' => t('Set the default volume (0 to 1; default is 0.5 to avoid overpowering screen reader audio).'),
    '#element_validate' => array('_ableplayer_youtube_validate_volume'),
  );

  $element['start_time'] = array(
    '#title' => t('Start time'),
    '#type' => 'textfield',
    '#size' => 6,
    '#description' => t('Time at which you want the audio to start playing (in seconds).'),
  );

  $element['seek_interval'] = array(
    '#title' => t('Seek interval'),
    '#type' => 'textfield',
    '#size' => 6,
    '#description' => t('Interval (in seconds) of forward and rewind buttons. By default, seek interval is calculated to be 1/10 of the duration of media.'),
  );

  $element['transcript_title'] = array(
    '#title' => t('Transcript title'),
    '#type' => 'textfield',
    '#size' => 18,
    '#description' => t('Override default transcript title (default is "Transcript", or "Lyrics" if the data-lyrics-mode attribute is present).'),
  );

  return $element;
}

function _ableplayer_youtube_validate_dimensions($element, &$form_state, $form) {
  if (!empty($element['#value']) && !preg_match('/^(auto|[0-9]*)$/', $element['#value'])) {
    form_error($element, t("The value entered for @dimension is invalid. Please insert a unitless integer for pixels or \"auto\".", array('@dimension' => $element['#title'],)));
  }
}

function _ableplayer_youtube_validate_volume($element, &$form_state, $form) {
  if (!empty($element['#value'])) {
    if (!is_numeric($element['#value']) || $element['#value'] < 0 || $element['#value'] > 1) {
      print_r($element['#value']);
      form_error($element, t("The value entered for @volume is invalid. Please insert a number between 0 and 1.", array('@volume' => $element['#title'],)));
    }
  }
}

function _ableplayer_youtube_validate_seek_interval($element, &$form_state, $form) {
  if (!empty($element['#value'])) {
    if (!is_numeric($element['#value'])) {
      form_error($element, t("The value entered for @seek is invalid. Please insert a number (seconds).", array('@seek' => $element['#title'],)));
    }
  }
}

function _ableplayer_youtube_validate_start_time($element, &$form_state, $form) {
  if (!empty($element['#value'])) {
    if (!is_integer($element['#value']) || $element['#value'] < 0) {
      form_error($element, t("The value entered for @start is invalid. Please insert a positive integer (seconds).", array('@start' => $element['#title'],)));
    }
  }
}
